const gulp  = require('gulp'),
    sass    = require('gulp-sass')
    cleanCSS = require('gulp-clean-css')
    concatJS = require('gulp-concat')
    concatCss = require("gulp-concat-css")
    htmlmin = require('gulp-htmlmin')
    uglify  = require('gulp-uglify')
    pump = require('pump')
    imagemin = require('gulp-imagemin')
    browserSync = require('browser-sync').create()
    reload = browserSync.reload;

    // gulp.task('minify-css', () => {
    //   return gulp.src(['app/css/*','!app/css/*.css.map'])
    //     .pipe(cleanCSS({compatibility: 'ie8'}))
    //     .pipe(gulp.dest('dist/css'));
    // });



    gulp.task('sass', function () {

        return gulp.src(['./node_modules/bootstrap/scss/bootstrap.scss','app/assets/scss/index.scss'])
        .pipe(sass())
        .pipe(concatCss("main.css"))
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist/css'));
    });


    gulp.task('minify', (cb) =>{
        pump([
            gulp.src(['app/js/*.js', './node_modules/bootstrap/dist/js/bootstrap.min.js']),
            concatJS('main.js'),
            uglify(),
            gulp.dest('dist/js')
        ],
        cb
    );
    });


    gulp.task('htmlminify', function() {
        return gulp.src('app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist/'));
    });

    gulp.task('imgmin', () => {
    gulp.src('app/assets/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/assets/img'))
    });

    gulp.task('fonts', () => {
      return gulp.src(['app/assets/fonts/*.*'])
            .pipe(gulp.dest('dist/fonts/'))
    })

    gulp.task('serve', ['sass', 'htmlminify', 'minify', 'imgmin', 'fonts'], () => {
        browserSync.init(['dist/css/*.css','dist/*.html','dist/js/*.js','app/img/*','fonts/*.*'], {
            port: 3030,
            ui: {
              port: 8080
            },
            server: {
                baseDir: 'dist',
            }
        });
    });


    gulp.task('watch', ['sass','htmlminify', 'minify', 'serve', 'imgmin', 'fonts'], function() {
        gulp.watch('app/assets/scss/*.scss', ['sass']);
        gulp.watch('app/*.html', ['htmlminify']);
        gulp.watch('app/js/*.js', ['minify']);
        gulp.watch('app/assets/img/*', ['imgmin']);
        gulp.watch('app/assets/fonts/*.*', ['fonts']);
    });

    gulp.task('default', ['watch']);
